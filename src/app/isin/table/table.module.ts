import { NgModule }      from '@angular/core';
import { CommonModule }  from '@angular/common';

import { RouterModule } from '@angular/router';

import { TableStatic } from './table.component.ts';

export const routes = [
  { path: '', component: TableStatic, pathMatch: 'full' }
];

@NgModule({
  imports: [ CommonModule, RouterModule.forChild(routes) ],
  declarations: [ TableStatic ]
})
export default class TableStaticModule {
  static routes = routes;
}
