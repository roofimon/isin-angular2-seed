import { NgModule }      from '@angular/core';
import { CommonModule }  from '@angular/common';

import { RouterModule } from '@angular/router';
import { SearchResultPage } from './search-result.component.ts';

export const routes = [
  { path: '', component: SearchResultPage, pathMatch: 'full' }
];

@NgModule({
  imports: [ CommonModule, RouterModule.forChild(routes) ],
  declarations: [ SearchResultPage ]
})
export default class SearchResultModule {
  static routes = routes;
}
