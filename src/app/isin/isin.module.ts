import { NgModule }      from '@angular/core';
import { CommonModule }  from '@angular/common';
import { FormsModule } from '@angular/forms';


import { RouterModule } from '@angular/router';
import { IsinPage } from './isin.component.ts';

import { JqSparklineModule } from '../components/sparkline/sparkline.module';
import { Ng2TableModule } from 'ng2-table';
import { ButtonsModule, DropdownModule, PaginationModule  } from 'ng2-bootstrap/ng2-bootstrap';


import { TableStatic } from './table/table.component';
import { SearchResult } from './search-result/search-result.component';

export const routes = [
  { path: '', component: IsinPage, pathMatch: 'full' },
  { path: 'static', component: TableStatic},
  { path: 'search-result', component: SearchResult}
];

@NgModule({
  imports: [ FormsModule, CommonModule, JqSparklineModule, Ng2TableModule, PaginationModule, RouterModule.forChild(routes) ],
  declarations: [ IsinPage, TableStatic, SearchResult ]
})
export default class AnotherModule {
  static routes = routes;
}
